package unit6;


public class MainClass {
	public static final String FORMAT_NUMBERS = "%4d";
	public static final String FORMAT_STRING = "%4s";
	public static final String FORMAT_NUMBERS_2 = "%3d     ";

	public static void main(String[] args) {

		printPatternA(6);
		System.out.println();
		printPatternB(6);
		System.out.println();
		printPatternC(6);
		System.out.println();
		printPatternD(6);
		System.out.println();
		printPatternE(7);
		System.out.println();
		printPatternF(7);
		System.out.println();
		printPatternG(7);
		System.out.println();
		printPatternH(7);
		System.out.println();
		printPatternI(7);
		System.out.println();
		printPatternJ(5);
		System.out.println();
		printPatternK(5);
		System.out.println();
		printPatternL(11);
		System.out.println();
		printPatternM(8);
		System.out.println();
		printPatternN(8);
		System.out.println();
		printPatternO(8);
		System.out.println();
		printPatternP(8);
		System.out.println();
		printPatternQ(7);
		System.out.println();
		printPatternR(7);
		System.out.println();
		printPatternS(7);
		System.out.println();
		printPatternT(7);
		System.out.println();
		printPatternU(7);

		printATriangle(8);
		System.out.println();
		printBTriangle(8);
		System.out.println();
		printCTriangle(8);
		System.out.println();
	}

	public static void printATriangle(int numRows) {
		int var;
		for (int i = 1; i <= numRows; i++) {
			for (int j = numRows - i; j >= 1; j--) {
				System.out.print(String.format(FORMAT_STRING, ""));
			}
			var = 1;
			for (int j = 1; j <= i; j++) {
				System.out.print(String.format(FORMAT_NUMBERS, var));
				var = var * 2;
			}
			var = var / 2;
			var = var / 2;
			for (int j = i; j > 1; j--) {
				System.out.print(String.format(FORMAT_NUMBERS, var));
				var = var / 2;
			}
			System.out.println();
		}
	}

	public static void printBTriangle(int numRows) {

		int[][] arr = new int[numRows][numRows];

		arr[0][0] = 1;
		arr[1][0] = 1;
		arr[1][1] = 1;
		System.out.println(String.format(FORMAT_NUMBERS, arr[0][0]));
		System.out.print(String.format(FORMAT_NUMBERS, arr[1][0]));
		System.out.println(String.format(FORMAT_NUMBERS, arr[1][1]));
		for (int row = 2; row < arr.length; row++) {
			for (int col = 0; col <= row; col++) {
				if (col == 0 || col == row) {
					arr[row][col] = 1;
				} else {
					arr[row][col] = arr[row - 1][col - 1] + arr[row - 1][col];
				}
				System.out.print(String.format(FORMAT_NUMBERS, arr[row][col]));
			}
			System.out.println();
		}
	}

	public static void printCTriangle(int numRows) {
		int[][] arr = new int[numRows][numRows];
		arr[0][0] = 1;
		arr[1][0] = 1;
		arr[1][1] = 1;
		for (int i = numRows - 1; i >= 1; i--) {
			System.out.print(String.format(FORMAT_STRING, ""));
		}
		System.out.println(String.format(FORMAT_NUMBERS_2, arr[0][0]));
		for (int i = numRows - 2; i >= 1; i--) {
			System.out.print(String.format(FORMAT_STRING, ""));
		}
		System.out.print(String.format(FORMAT_NUMBERS_2, arr[1][0]));
		System.out.println(String.format(FORMAT_NUMBERS_2, arr[1][1]));
		for (int row = 2; row < arr.length; row++) {
			boolean first = true;
			for (int col = 0; col <= row; col++) {
				if (col == 0 || col == row) {
					arr[row][col] = 1;
				} else {
					arr[row][col] = arr[row - 1][col - 1] + arr[row - 1][col];
				}
				if (first) {
					for (int j = numRows - (row + 1); j >= 1; j--) {
						System.out.print(String.format(FORMAT_STRING, ""));
					}
					first = false;
				}
				System.out.print(String.format(FORMAT_NUMBERS_2, arr[row][col]));
			}
			System.out.println();
		}
	}

	public static void printPatternA(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print("# ");
			}
			System.out.println();
		}
	}

	public static void printPatternB(int size) {
		for (int i = size; i >= 1; i--) {
			for (int j = 1; j <= i; j++) {
				System.out.print("# ");
			}
			System.out.println();
		}
	}

	public static void printPatternC(int size) {
		int count = 0;
		for (int i = size; i >= 1; i--) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print("# ");
			}
			System.out.println();
			count++;
		}
	}

	public static void printPatternD(int size) {
		int count = size - 1;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print("# ");
			}
			System.out.println();
			count--;
		}
	}

	public static void printPatternE(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size; j++) {
				if (i == 1 || i == size) {
					System.out.print("# ");
					if (j == size) {
						System.out.println();
					}
				} else {
					if (j == 1 || j == size) {
						System.out.print("# ");
						if (j == size) {
							System.out.println();
						}
					} else {
						System.out.print("  ");
					}
				}
			}
		}
	}

	public static void printPatternF(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size; j++) {
				if (i == 1 || i == size) {
					System.out.print("# ");
					if (j == size) {
						System.out.println();
					}
				} else {
					if (i == j) {
						System.out.print("# ");
					} else {
						System.out.print("  ");
						if (j == size) {
							System.out.println();
						}
					}
				}
			}
		}
	}

	public static void printPatternG(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size; j++) {
				if (i == 1 || i == size) {
					System.out.print("# ");
					if (j == size) {
						System.out.println();
					}
				} else {
					if ((i + j) == (size + 1)) {
						System.out.print("# ");
					} else {
						System.out.print("  ");
						if (j == size) {
							System.out.println();
						}
					}
				}
			}
		}
	}

	public static void printPatternH(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size; j++) {
				if (i == 1 || i == size) {
					System.out.print("# ");
					if (j == size) {
						System.out.println();
					}
				} else {
					if ((i + j) == (size + 1) || (i == j)) {
						System.out.print("# ");
					} else {
						System.out.print("  ");
						if (j == size) {
							System.out.println();
						}
					}
				}
			}
		}
	}

	public static void printPatternI(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size; j++) {
				if (i == 1 || i == size) {
					System.out.print("# ");
					if (j == size) {
						System.out.println();
					}
				} else {
					if ((i + j) == (size + 1) || (i == j) || j == 1 || j == size) {
						System.out.print("# ");
						if (j == size) {
							System.out.println();
						}
					} else {
						System.out.print("  ");
					}
				}
			}
		}
	}

	public static void printPatternJ(int size) {
		int count = 0;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= size - count; j++) {
				System.out.print("# ");
			}
			for (int j = size - (count + 1); j >= 1; j--) {
				System.out.print("# ");
			}
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			count++;
			System.out.println();
		}
	}

	public static void printPatternK(int size) {
		int count = size - 1;
		for (int i = 1; i <= size; i++) {
			for (int j = count; j >= 1; j--) {
				System.out.print("  ");
			}
			for (int j = 1; j <= size - count; j++) {
				System.out.print("# ");
			}
			for (int j = 1; j <= i - 1; j++) {
				System.out.print("# ");
			}
			for (int j = count; j >= 1; j--) {
				System.out.print("  ");
			}
			count--;
			System.out.println();
		}
	}

	public static void printPatternL(int size) {
		int count = (size / 2) - 1;
		for (int i = 1; i <= (size / 2); i++) {
			System.out.print("  ");
			for (int j = count; j >= 1; j--) {
				System.out.print("  ");
			}
			for (int j = 1; j <= (size / 2) - count; j++) {
				System.out.print("# ");
			}
			for (int j = 1; j <= i - 1; j++) {
				System.out.print("# ");
			}
			for (int j = count; j >= 1; j--) {
				System.out.print("  ");
			}
			count--;
			System.out.println();
		}
		printPatternJ((size / 2) + 1);

	}

	public static void printPatternM(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}

	public static void printPatternN(int size) {
		int count = 0;
		for (int i = size; i >= 1; i--) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
			count++;
		}
	}

	public static void printPatternO(int size) {
		int count = size - 1;
		int constant = 2;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= i; j++) {
				System.out.print(constant - j + " ");
			}
			constant++;
			System.out.println();
			count--;
		}
	}

	public static void printPatternP(int size) {
		for (int i = size; i >= 1; i--) {
			for (int j = 0; j < i; j++) {
				System.out.print(i - j + " ");
			}
			System.out.println();
		}
	}

	public static void printPatternQ(int size) {
		int count = size - 1;
		for (int i = 1; i <= size; i++) {
			for (int j = count; j >= 1; j--) {
				System.out.print("  ");
			}
			for (int j = 1; j <= size - count; j++) {
				System.out.print(j + " ");
			}
			for (int j = 1; j <= i - 1; j++) {
				System.out.print(i - j + " ");
			}
			for (int j = count; j >= 1; j--) {
				System.out.print("  ");
			}
			count--;
			System.out.println();
		}
	}

	public static void printPatternR(int size) {
		int count = 0;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= size - count; j++) {
				System.out.print(j + " ");
			}
			for (int j = size - (count + 1); j >= 1; j--) {
				System.out.print(j + " ");
			}
			for (int j = 1; j <= count; j++) {
				System.out.print("  ");
			}
			count++;
			System.out.println();
		}
	}

	public static void printPatternS(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			for (int j = 1; j <= size - i; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j <= size - i - 1; j++) {
				System.out.print("  ");
			}
			for (int j = i; j >= 1; j--) {
				if (j != size) {
					System.out.print(j + " ");
				}
			}
			System.out.println();
		}
	}

	public static void printPatternT(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size + 1 - i; j++) {
				System.out.print(j + " ");
			}
			for (int j = 1; j < i; j++) {
				System.out.print("  ");
			}
			for (int j = 1; j < i - 1; j++) {
				System.out.print("  ");
			}
			for (int j = size - i + 1; j >= 1; j--) {
				if (j != size) {
					System.out.print(j + " ");
				}
			}
			System.out.println();
		}
	}

	public static void printPatternU(int size) {
		for (int i = 1; i <= size; i++) {
			for (int j = i; j <= size - 1; j++) {
				System.out.print("  ");
			}
			for (int j = i; j < i * 2; j++) {
				if (j >= 10) {
					System.out.print(j - 10 + " ");
				} else {
					System.out.print(j + " ");
				}
			}
			for (int j = (i * 2) - 2; j >= i; j--) {
				if (j >= 10) {
					System.out.print(j - 10 + " ");
				} else {
					System.out.print(j + " ");
				}
			}

			System.out.println();
		}
	}

}
